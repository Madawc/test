# First, implement scraping of links for 1 simple url
# Then, loop over a list
# Find a way to give a list as a cmd line argument

# I know little about python but let's dig into it
import time
# requests module for http based operations
import requests
# regex module
import re
# argparse to allow commandline modules
import argparse
# json manipulation
import json
# url parsing to make it easier to get path and domains
from urllib.parse import urlparse
# BeautifulSoup module for html parsing
from bs4 import BeautifulSoup

# This whole block is inspired by : https://docs.python.org/3/library/argparse.html
# Struggled here for like 15 to 30 minutes before trying something
# Init parser
parser = argparse.ArgumentParser()
# Add my options here with number of args, choices, help, mandatory or not
parser.add_argument("-u", "--url", action='append', nargs='+', help="url to scrap should include scheme and quotes eg. \"https://example.org\"", required=True)
parser.add_argument("-o", "--output", choices={"stdout", "json"}, help="output format (json, stdout)", required=True)
# Get all the arguments from commandline
args = parser.parse_args()

# Function that exctract all links from a list of url given as parameter
def link_scrap(*url_list):
  
  # Loop over a list of url instead of just one
  for i in url_list:
    # Get on url
    reqs = requests.get(i)

    # Print content with correct parser (reqs.content instead of reqs.text to avoid encoding problems, see : https://realpython.com/beautiful-soup-web-scraper-python/#step-3-parse-html-code-with-beautiful-soup)
    result = BeautifulSoup(reqs.content, "html.parser")

    # initialize var to store data for json output
    data = {}

    # Loop over every link contained in content and print them (get all the <a> tags and then filter on href) see https://scrapeops.io/python-web-scraping-playbook/python-beautifulsoup-findall/
    for link in result.find_all('a'):
    
      # Store link into variable for future operation
      u = link.get('href')

      # Regex to match only those starting with either http or https scheme (avoid noise) - thanks to https://regexr.com/ for the die and retry part
      match_url = re.search("^https?://", u)
      
      # If it matches and output is set to stdout then I simply print it as it comes
      if match_url and args.output == 'stdout':
        print(u)

      elif match_url and args.output == 'json':

        # Parsing the part we're interested in to respect format
        scheme = urlparse(u).scheme
        hostname = urlparse(u).hostname
        key   = scheme+'://'+hostname
        value = urlparse(u).path

        # This was the most challenging part of the whole script imho, had to go through multiple resources, including chatgpt to understand how to manipulate a dictionary in python
        # Then the whole parsing was made by myself but the struggle was on the storage part where I append value or not into an already existing key
        # Been stuck here for at least 45 minutes
        # Add key (domain) only if it's not already in data structure
        if key not in data:
          data[key] = []
        
        # Append value here because we expect the domain to be set, use the same logic as domain here to avoid duplicate paths
        if value not in data[key]:
          data[key].append(value)

        # Dump dictionary into json
        json_data = json.dumps(data, indent=2)
        print(json_data)
    
# Call function
for i in args.url:
  # Magic parsing here to remove brackets and quotes from element in list
  link_scrap(', '.join(i))

# Add endless sleep at the end
while True:
  time.sleep(1)

# Total time: approx ~3h without counting coffees