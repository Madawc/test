# First way (where file contains the list given) I assumed there was a typo at "sub.ads.faCebok.com" -> it was meant to be 'Facebook' instead, one day, maybe, I'll know the truth
# print list, print all uppercase character to lowercase character, remove all dots that end a line, remove 'http://' scheme, remove 'https://' scheme, filter by regex matching pattern, sort alphabeticaly, print uniq occurence only 
cat list |tr '[:upper:]' '[:lower:]'|sed 's/\.$//'|sed 's/http:\/\///'|sed 's/https:\/\///'|grep -Po '[^.]+\.[^.]+$'|sort|uniq

# Second way - Remove all dots that end a line, print last two field if the number of fields is greater than 1, remove 'http://' scheme, print to lowercase, sort alphabeticaly, print uniq occurence only
sed 's/\.$//' list |awk -F '.' '{if (NF > 1) print $(NF-1)"."$NF}'|sed 's/http:\/\///'|sed 's/\(.*\)/\L\1/'|sort|uniq

# Third way - Reverse the file, remove all dots that start a line, print to lowercase, remove residual scheme, filter by unicity, reverse the file again
rev list |sed 's/^\.//'|cut -d '.' -f1-2|awk '{print tolower($0)}'|sed 's/\/\/:ptth//'|sort -u|rev
