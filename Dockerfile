# Base Image
FROM python:3.9-alpine

# Create user for future use
RUN adduser --system --no-create-home python
WORKDIR /usr/src/app

# Basic work here
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY scraper.py .
# Run as non-root
USER python
# Use entrypoint instead of CMD here for argparse to work, -u option is necessary to print to stdout
ENTRYPOINT [ "python", "-u", "./scraper.py" ]
